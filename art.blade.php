@extends('all')
@section('halaman')
<!-- ======= Art Section ======= -->
<section id="art" class="art">
    <div class="container">

      <div class="section-title">
        <h2>ART</h2>
        <p>Beberapa fakta tentang seni</p>
      </div>

      <div class="row">
        <div class="col-lg-6">
          <div class="art-item">
            <h4>Keahlian</h4>
            <p><em> Seni merupakan suatu keahlian seseorang dalam membuat karya yang bermutu akan dilihat dari segi kehalusannya, keindahannya, fungsinya, bentuknya, maknanya, dan lain sebagainya. </em></p>
          </div>
          <div class="art-item">
            <h4>Daya Cipta</h4>
            <p><em>Seni dapat diartikan sebagai suatu hasil ciptaan manusia yang mengandung unsur keindahan dan juga dapat mempengaruhi perasaan orang lain.</em></p>
          </div>
          <div class="art-item">
            <h4>Beragam</h4>
            <p><em>Seni tidak bisa dikategorikan hanya dengan 1 bidang, melainkan banyak bidang dalam kehidupan.</em></p>
          </div>
        </div>
        <div class="col-lg-6">
          <h3 class="art-title">Macam-macam Seni</h3>
          <div class="art-item">
            <p>
            <ul>
              <li>Seni Gerak</li>
              <li>Seni Rupa</li>
              <li>Seni Music</li>
              <li>Seni Sastra</li>
            </ul>
            </p>
          </div>
        </div>
      </div>

    </div>
  </section><!-- End Art Section -->

    
@endsection