@extends('new.all')
@section('halaman')
    <!-- ======= About Section ======= -->
  <section id="about" class="about">

    <!-- ======= About Me ======= -->
    <div class="about-me container">

      <div class="section-title">
        <h2>About</h2>
        <p>My Profile</p>
      </div>

      <div class="row">
        <div class="col-lg-4" data-aos="fade-right">
          <img src="{{asset('front/img/muka.jpg')}}" class="img-fluid" alt="">
        </div>
        <div class="col-lg-8 pt-4 pt-lg-0 content" data-aos="fade-left">
          <h3>PROFILE</h3>
          <p class="fst-italic">
            Berikut tentang profile singkat saya
          </p>
          <div class="row">
            <div class="col-lg-9">
              <ul>
                <li><i class="bi bi-chevron-right"></i> <strong>Fullname:</strong> <span>Putu Deva Tarinda Novani</span></li>
                <li><i class="bi bi-chevron-right"></i> <strong>Nickname:</strong> <span>Deva</span></li>
                <li><i class="bi bi-chevron-right"></i> <strong>Tempat,tanggal lahir:</strong> <span>Penatahan,02 November 2000</span></li>
                <li><i class="bi bi-chevron-right"></i> <strong>Pekerjaan</strong> <span>Mahasiswa</span></li>
                <li><i class="bi bi-chevron-right"></i> <strong>Alamat:</strong> <span>Sanggulan, Kediri, Tabanan</span></li>
                <li><i class="bi bi-chevron-right"></i> <strong>Telepon:</strong> <span>+6281558319353</span></li>
                <li><i class="bi bi-chevron-right"></i> <strong>Email:</strong> <span>devatarinda51@gmail.comn</span></li>
              </ul>
            </div>
          </div>
          <p>
          </p>
        </div>
      </div>

    </div><!-- End About Me -->

    <!-- ======= Skills  ======= -->
    <div class="skills container">

      <div class="section-title">
        <h2>Skills</h2>
      </div>

      <div class="row skills-content">

        <div class="col-lg-6">

          <div class="progress">
            <span class="skill">Design <i class="val">80%</i></span>
            <div class="progress-bar-wrap">
              <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100">
              </div>
            </div>
          </div>

          <div class="progress">
            <span class="skill">Coding <i class="val">20%</i></span>
            <div class="progress-bar-wrap">
              <div class="progress-bar" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
              </div>
            </div>
          </div>

          <div class="progress">
            <span class="skill">Art <i class="val">90%</i></span>
            <div class="progress-bar-wrap">
              <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
              </div>
            </div>
          </div>

        </div>

        <div class="col-lg-6">

          <div class="progress">
            <span class="skill">Attitude <i class="val">95%</i></span>
            <div class="progress-bar-wrap">
              <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100">
              </div>
            </div>
          </div>

          <div class="progress">
            <span class="skill"> Photograpy <i class="val">80%</i></span>
            <div class="progress-bar-wrap">
              <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100">
              </div>
            </div>
          </div>

          <div class="progress">
            <span class="skill"> Analisis <i class="val">60%</i></span>
            <div class="progress-bar-wrap">
              <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
              </div>
            </div>
          </div>

        </div>

      </div>

    </div><!-- End Skills -->

    

    <!-- ======= Testimonials ======= -->
    <div class="testimonials container">

      <div class="section-title">
        <h2>Hobby</h2>
      </div>

      <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
        <div class="swiper-wrapper">

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Melukis
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="{{asset('front/img/testimonials/art.jpg')}}" class="testimonial-img" alt="">
              <h3>Melukis adalah suatu hal yang menyenangkan</h3>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Bermain musik
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="{{asset('front/img/testimonials/musik.png')}}" class="testimonial-img" alt="">
              <h3>Sesuatu yang bisa dilakukan untuk menghilangkan rasa jenuh</h3>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
               Mendengarkan lagu
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="{{asset('front/img/testimonials/hd.png')}}" class="testimonial-img" alt="">
              <h3>Salah satu cara untuk menenangkan hati</h3>
            </div>
          </div><!-- End testimonial item -->

          <div class="swiper-slide">
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Tidur
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="{{asset('front/img/testimonials/sleep.jpg')}}" class="testimonial-img" alt="">
              <h3>Tidur membantu kita untuk mengembalikan energi</h3>
            </div>
          </div><!-- End testimonial item -->

        </div>
        <div class="swiper-pagination"></div>
      </div>

      <div class="owl-carousel testimonials-carousel">

      </div>

    </div><!-- End Testimonials  -->

  </section><!-- End About Section -->
    
@endsection

