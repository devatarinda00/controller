@extends('all')
@section('halaman')
<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="portfolio">
    <div class="container">

      <div class="section-title">
        <h2>Portfolio</h2>
        <p>My Galeri</p>
      </div>

      <div class="row">
        <div class="col-lg-12 d-flex justify-content-center">
          <ul id="portfolio-flters">
            <li data-filter="*" class="filter-active">All</li>
            <li data-filter=".filter-art">Flower</li>
            <li data-filter=".filter-fotografi">Fotografi</li>
          </ul>
        </div>
      </div>

      <div class="row portfolio-container">

        <div class="col-lg-4 col-md-6 portfolio-item filter-art">
          <div class="portfolio-wrap">
            <img src="{{asset('front/img/portfolio/art1.png')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Flower 1</h4>
              <p>Flower</p>
              <div class="portfolio-links">
                <a href="{{asset('front/img/portfolio/art1.png')}}" data-gallery="portfolioGallery" class="portfolio-lightbox"
                  title="Art 1"><i class="bx bx-plus"></i></a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-fotografi">
          <div class="portfolio-wrap">
            <img src="{{asset('front/img/portfolio/foto1.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Fotografi 1</h4>
              <p>Fotografi</p>
              <div class="portfolio-links">
                <a href="{{asset('front/img/portfolio/foto1.jpg')}}" data-gallery="portfolioGallery" class="portfolio-lightbox"
                  title="Fotografi 1"><i class="bx bx-plus"></i></a>

              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-art">
          <div class="portfolio-wrap">
            <img src="{{asset('front/img/portfolio/art2.jpeg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Flower 2</h4>
              <p>Flower</p>
              <div class="portfolio-links">
                <a href="{{asset('front/img/portfolio/art2.jpeg')}}" data-gallery="portfolioGallery" class="portfolio-lightbox"
                  title="Art 2"><i class="bx bx-plus"></i></a>

              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-fotografi">
          <div class="portfolio-wrap">
            <img src="{{asset('front/img/portfolio/foto2.jpeg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Fotografi 2</h4>
              <p>Fotografi</p>
              <div class="portfolio-links">
                <a href="{{asset('front/img/portfolio/foto2.jpeg')}}" data-gallery="portfolioGallery" class="portfolio-lightbox"
                  title="Fotografi 2"><i class="bx bx-plus"></i></a>

              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-fotografi">
          <div class="portfolio-wrap">
            <img src="{{asset('front/img/portfolio/foto3.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Fotografi 3</h4>
              <p>Fotografi</p>
              <div class="portfolio-links">
                <a href="{{asset('front/img/portfolio/foto3.jpg')}}" data-gallery="portfolioGallery" class="portfolio-lightbox"
                  title="Fotografi 3"><i class="bx bx-plus"></i></a>

              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-art">
          <div class="portfolio-wrap">
            <img src="{{asset('front/img/portfolio/art3.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Flower 3</h4>
              <p>Flower</p>
              <div class="portfolio-links">
                <a href="{{asset('front/img/portfolio/art3.jpg')}}" data-gallery="portfolioGallery" class="portfolio-lightbox"
                  title="Art 3"><i class="bx bx-plus"></i></a>

              </div>
            </div>
          </div>
        </div>

      </div>

    </div>
  </section><!-- End Portfolio Section -->
    
@endsection