<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Profile</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{asset('front/img/favicon.png')}}" rel="icon">
  <link href="{{asset('front/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link
    href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
    rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{asset('front/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('front/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{asset('front/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('front/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
  <link href="{{asset('front/vendor/remixicon/remixicon.css')}}}" rel="stylesheet">
  <link href="{{asset('front/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('front/css/style.css')}}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Personal - v4.5.0
  * Template URL: https://bootstrapmade.com/personal-free-resume-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

    <!-- ======= Header ======= -->
    <header id="header">
      <div class="container">
  
        <h1><a href="index.html">Putu Deva Tarinda Novani</a></h1>
        
      
       
        <nav id="navbar" class="navbar">
          <ul>
            <li><a class="nav-link active" href="/header">Home</a></li>
            <li><a class="nav-link" href="/about">About</a></li>
            <li><a class="nav-link" href="/resume">Art</a></li>
            <li><a class="nav-link" href="/portfolio">Portfolio</a></li>
            <li><a class="nav-link" href="/contact">Contact</a></li>
          </ul>
          <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->
  
      </div>
    </header><!-- End Header -->

    @yield('halaman')

    <!-- Vendor JS Files -->
  <script src="{{asset('front/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('front/vendor/glightbox/js/glightbox.min.js')}}"></script>
  <script src="{{asset('front/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{asset('front/vendor/php-email-form/validate.js')}}"></script>
  <script src="{{asset('front/vendor/purecounter/purecounter.js')}}"></script>
  <script src="{{asset('front/vendor/swiper/swiper-bundle.min.js')}}"></script>
  <script src="{{asset('front/vendor/waypoints/noframework.waypoints.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{asset('front/js/main.js')}}"></script>

</body>
</html>